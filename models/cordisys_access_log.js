"use strict";

//const DataTypes = require("sequelize");
//const configs = require("../configs/configs");
//const sequelize = configs.sequelize;

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cordisys_access_log', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    accesstime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    browers: {
      type: DataTypes.STRING,
      allowNull: true
    },
    os: {
      type: DataTypes.STRING,
      allowNull: true
    },
    ip: {
      type: DataTypes.STRING,
      allowNull: true
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'cordisys_access_log'
  });
};
