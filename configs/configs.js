"use strict";
const Sequelize = require("sequelize");
const path = require("path");
const root = path.dirname(__dirname); // Located in the root directory of the project

module.exports.path = {};
module.exports.path.static = path.join(root, "public");
module.exports.path.views = path.join(root, "views");

// load configs.json
const configs = require("../configs");

/*      database config
 *      use Sequelize ORM
 */

const dbHost = configs.postgres.host;
const dbPort = configs.postgres.port;
const dbUsername = configs.postgres.username;
const dbPassword = configs.postgres.password;
const dbName = configs.postgres.dbName;

const sequelize = new Sequelize(dbName, dbUsername, dbPassword, {
        host: dbHost,
        dialect: "postgres",
        port: dbPort,
        pool: {
                max: 5,
                min: 0,
                idle: 10000
        },
        define: {
                freezeTableName: true,
                timestamps: false
        }
});

module.exports.sequelize = sequelize;


