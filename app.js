'use strict';
//const main = require('./controllers/Main');
const compress = require('koa-compress');
const logger = require('koa-logger');
const serve = require('koa-static');
//const route = require('koa-route');
const koa = require('koa');
//const path = require('path');
const app = module.exports = koa();

// Logger
app.use(logger());

//app.use(route.get('/', main.home));
//app.use(route.get('/messages', messages.list));
//app.use(route.get('/messages/:id', messages.fetch));
//app.use(route.post('/messages', messages.create));
//app.use(route.get('/async', messages.delay));
//app.use(route.get('/promise', messages.promise));

// Serve static files
const configs = require("./configs/configs");
const staticFilePath = configs.path.static;
app.use(serve(staticFilePath));

// Compress
app.use(compress());

// routes
app.use(require("./routes/routes"));


if (!module.parent) {
  app.listen(3000);
  console.log('listening on port 3000');
}
