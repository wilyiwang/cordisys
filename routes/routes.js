"use strict";
const router = require("koa-router")();
const configs = require("../configs/configs");

const main = require("../controllers/Main");


router.get("/", main.home);


module.exports = router.routes();


