'use strict';
//const router = require("koa-router")();
const configs = require("../configs/configs");
const views = require("co-views");
const parse = require("co-body");
const viewsPath = configs.path.views;

const DataTypes = require("sequelize");
const sequelize = configs.sequelize;

const Accesslog = require("../models/cordisys_access_log")(sequelize,DataTypes);

const render = views(viewsPath, {
	map: {
		html: "swig"
	}
});

module.exports.home = function *home(ctx) {
//  this.body = yield render('main');
   let accesslogs = yield Accesslog.findAll({
                attributes: ["id", "userid", "accesstime", "browers", "os","ip","address"],
                order: [
                    ["accesstime", "DESC"]
                ]
              //  offset: (current - 1) * limit,
               // limit: limit
            });
  this.body = yield render("main", {
                title: "Accesslog",
                accesslogs: accesslogs
            });
            return;

};


